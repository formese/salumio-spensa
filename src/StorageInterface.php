<?php

namespace SalumIo\Spensa;

interface StorageInterface
{
    public function size();
    public function get($key);
    public function contains($key);
}
